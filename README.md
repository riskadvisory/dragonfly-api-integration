# Api Integration

- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installing](#installing)
- [Making first API call](#api-call)

## Getting Started

These instructions will help you integrate with DragonFly APIs.

### Prerequisites

What things you need to install the software and how to install them

```
Python 3.7 or greater
Virtualenv
```

### Installing

Cloning the repo Make virtualenv and install requirements

```shell
pip install -r requirements.txt
```
Copy .env.example file to .env and populate information in .env file.
```shell
cp .env.example .env
```
Update .env file with DRAGONFLY_HOST(given by dragonfly), COMPANY_EMAIL(email used to signup), COUNTRY(Where your company is based 2 letter code)
STATE_OR_PROVINCE_NAME(if known), ORGANIZATION_NAME(your company name) and ORGANIZATION_UNIT_NAME(If known)


### Creating Public and private keys
Call python file to generate public private key pairs. This will be used later to 
create JWT token. Please share the public.key file with Dragonfly, so you can make successful calls.
```shell
python create_cert_1.py
```
### Making first API call

After installing requirements and updating env file use get_incidents_json_2.py to call incidents API. Rest of the apis follow
same direction. Swagger of APIs can be found on the host as well.
```shell
python get_incidents_json_2.py
```
All APIs also support csv streaming of data as well.
```shell
python get_incidents_csv_3.py
```
To download swagger call following python file. It will be saved as swagger.yaml file
```shell
python get_swagger_4.py
```
