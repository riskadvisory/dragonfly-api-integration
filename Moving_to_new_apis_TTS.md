# Transitional guide of new API's (TT)

## Authentication

#### Dragonfly apis uses JWT based authentication.

Go through `README.md` for generating JWT token.

Pass JWT token in authorization header in your request to the endpoint.

____

## Location API's

You can access data related to cities, countries and regions.

#### Cities
List all the cities. 

##### Old vs New API curl command.

Their was no endpoint for cities in the old API. Following is the curl command for the new one.

e.g ` GET https://client-apis.dragonflyintelligence.com/core/v1/cities `

#### Example
This is a sample result of cities endpoint.
```json
{
   "data":[
      {
         "country":{
            "code":null,
            "id":821,
            "name":"Switzerland",
            "region":{
               "id":263,
               "name":"Europe"
            }
         },
         "id":926,
         "name":"Zurich"
      },
      {
         "country":{
            "code":null,
            "id":174,
            "name":"China",
            "region":{
               "id":43,
               "name":"Asia Pacific"
            }
         },
         "id":924,
         "name":"Zhengzhou"
      }
   ]
}
```

##### Comparision of New API fields with Old ones.
_~~The Old one did not have any Endpoint for cities.~~_

#### Countries
List all the countries.

##### Old vs New API curl command.
| Old                                                                                                                 | New                                  |
|---------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| `curl -X GET https://api.terrorismtracker.com/api/v1/countries -H 'Accept: application/json' -H 'API-KEY: API_KEY'` | `curl GET https://client-apis.dragonflyintelligence.com/core/v1/countries` |

##### Comparison between new and old fields
| New API field                                               | Old API field                                        | Description                                                                                |
|-------------------------------------------------------------|------------------------------------------------------|--------------------------------------------------------------------------------------------|
| `"region": {"id": 248, "name": "East &amp; Southern Africa"}` |  `"geographical_region": {"id": 0, "name": "string"}}` | The Old API returns "region" field as an object. The New API returns "geographical_region" |
|                                                             |                                                      |                                                                                            |
##### Newly added fields in New API

| Field | Description             |
|-------|-------------------------|
| code  | A code for the country. |

##### Example result
Two countries result returned by /countries endpoint.


```json
{
   "data":[
      {
         "code":null,
         "id":925,
         "name":"Zimbabwe",
         "region":{
            "id":248,
            "name":"East &amp; Southern Africa"
         }
      },
      {
         "code":null,
         "id":922,
         "name":"Zambia",
         "region":{
            "id":248,
            "name":"East &amp; Southern Africa"
         }
      }
   ]
}
```
#### Regions
List all the regions.

##### Old vs New API curl command.

| Old                                                                                                                            | New                                |
|--------------------------------------------------------------------------------------------------------------------------------|------------------------------------|
| `curl -X GET https://api.terrorismtracker.com/api/v1/geographical_regions -H 'Accept: application/json' -H 'API-KEY: API_KEY'` | `curl GET https://client-apis.dragonflyintelligence.com/core/v1/regions` |

##### Comparison between new and old fields

| New API field                                     | Old API field                                                           | Description                                                                                           |
|---------------------------------------------------|-------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| `{"id": 905, "name": "West &amp; Central Africa"}` | `{"id": 0, "name": "string", "countries": [{"id": 0, "name": "string"}]}` | THe Old API returns region's id, name and nested countries. The New API returns region's id and name. |

##### Example result
Two regions returned by /regions endpoint

```json
{
   "data":[
      {
         "id":905,
         "name":"West &amp; Central Africa"
      },
      {
         "id":931,
         "name":"Sub-Saharan Africa"
      }
   ]
}
```
---

## Incidents
Lists all incidents which have occured between specified dates. Filter result using country_id or country_name. Paginate through results using _page_ and _limit_ parameters

##### Old vs New API curl command.

| Old                                                                                                                 | New                                                                                        |
|---------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------|
| `curl -X GET https://api.terrorismtracker.com/api/v1/incidents -H 'Accept: application/json' -H 'API-KEY: API_KEY'` | `GET https://client-apis.dragonflyintelligence.com/core/v1/incidents?from_date=1-1-2015&to_date=1-1-2016&country_names=France` |

#### Query Parameters
| Parameter     | In    | Description                         | Type           | Required |
|---------------|-------|-------------------------------------|----------------|----------|
| page          | query | Page number of results              | integer        | False    |
| limit         | query | Number of results per page          | integer        | False    |
| from_date     | query | Results newer than given date       | string         | True     |
| to_date       | query | Results older than given date       | string         | True     |
| sort          | query | Single field sort by.               | string         | False    |
| order         | query | Ordering to use for the sort field  | string         | False    |
| country_ids   | query | Country ids to filter results by    | Array[integer] | False    |
| country_names | query | Country names to filter results by. | Array[string]  | False    |
 
 #### Example
 Query incidents occured in **France** between **2015 and 2016**.
 
 `GET https://client-apis.dragonflyintelligence.com/core/v1/incidents?from_date=1-1-2015&to_date=1-1-2016&country_names=France`

##### Example result
 This is an incident returned by the API.
 
 ```json
{
   "data":[
      {
         "accuracy":"100-200m",
         "area":"Valence, Drome, Rhone-Alpes",
         "country":{
            "code":null,
            "id":270,
            "name":"France",
            "region":{
               "id":263,
               "name":"Europe"
            }
         },
         "end_date":"2016-01-01",
         "end_time":"",
         "end_time_lookup":"unspecified",
         "id":34793,
         "latitude":"44.93476",
         "longitude":"4.89854",
         "perpetrators":[
            {
               "country":null,
               "name":"Unknown - Jihadists"
            }
         ],
         "source_urls":[
            "http://www.aljazeera.com/news/2016/01/man-rams-car-soldiers-protecting-mosque-france-160102034438699.html",
            "and ",
            "http://www.bbc.co.uk/news/world-europe-35211213"
         ],
         "specific_target":null,
         "start_date":"2016-01-01",
         "start_time":"",
         "start_time_lookup":"unspecified",
         "suicide":false,
         "summary":"A French citizen of Tunisian descent wounded a soldier in a vehicular attack outside a mosque in Valence, Drome, on 1 January. Media reports indicate that the attacker hit one soldier before the security forces, who guarded the mosque at the time of the incident, were able to stop the attacker by shooting and seriously injuring him. The attacker had reportedly no links to terrorist organisations, albeit jihadist propaganda images were found on his computer.",
         "targets":[
            "Military"
         ],
         "terrorists_killed":0,
         "victims":{
            "by_nationality":[
               {
                  "deaths":0,
                  "hostages":0,
                  "injured":1,
                  "nationality":"France"
               }
            ],
            "totals":{
               "deaths":0,
               "hostages":0,
               "injured":1
            }
         },
         "weapons":[
            "Other/Unclear"
         ]
      }
   ]
}
```
 
 #### CSV Format
 The new API can return incidents in CSV format also. Following are the headers of the comma separated values.
 - id
 - specific_target
 - suicide
 - terrorists_killed
 - summary
 - source_urls
 - source_names
 - start_date
 - end_date
 - start_time
 - start_time_lookup
 - end_time
 - end_time_lookup
 - latitude
 - longitude
 - accuracy
 - area
 - country_code
 - country
 - region
 - targets
 - weapons
 - total_deaths
 - total_hostages
 - total_injured
 - deaths
 - hostages
 - injured
 - perpetrators  
 
 #### Example
 An incident returned in CSV format.
 
```json
{
  "text/csv": "id,specific_target,suicide,terrorists_killed,summary,source_urls,source_names,start_date,end_date,start_time,start_time_lookup,end_time,end_time_lookup,latitude,longitude,accuracy,area,country_code,country,region,targets,weapons,total_deaths,total_hostages,total_injured,deaths,hostages,injured,perpetrators 123,Army,t,58,\"Unknown terrorists ambushed a military patrol.\",\"https://www.journalducameroun.com/en/122; https://www.facebook.com/tebopostmedia/12\",\"Journal du Cameroun; TeboPost Media\",2018-02-24,2018-02-25,\"Midnight\",\"night\",\"Afternoon\",\"afternoon\",31.334,69.146843,\"100-200m\",\"Elazig\",MAR,Foo,\"Europe\",\"Police and prisons; Military; Diplomatic\",\"Mortars; Grenade; Rockets\",10,2,22,\"Cameroon: 1; Pakistan: 1; Hungry: 1\",\"Cameroon: 22; Pakistan: 1; Hungry: 67\",\"Cameroon: 3432; Pakistan: 121; Hungry: 1\",\"'Unknown: Unknown - Separatists/Nationalists; Cameroon: Unknown - Separatists/Nationalists; Cameroon: Jihad\" 124,Army,t,12,\"Unknown terrorists ambushed a military patrol.\",\"https://www.journalducameroun.com/en/122; https://www.facebook.com/tebopostmedia/12\",\"Journal du Cameroun; TeboPost Media\",2018-02-24,2018-02-25,\"Midnight\",\"night\",\"Afternoon\",\"afternoon\",31.334,69.146843,\"100-200m\",\"Elazig\",MAR,Foo,\"Europe\",Police and prisons; Military; Diplomatic,\"Mortars; Grenade; Rockets\",10,2,22,\"Cameroon: 1; Pakistan: 1; Hungry: 1\",\"Cameroon: 22; Pakistan: 1; Hungry: 67\",\"Cameroon: 3432; Pakistan: 121; Hungry: 1\",\"'Unknown: Unknown - Separatists/Nationalists; Cameroon: Unknown - Separatists/Nationalists; Cameroon: Jihad\"\n"
}
```
 
 #### Comparision of different fields
 This is a comparision of those old fields and new ones which are different from each other.
 
| New API fields                                                                                                                                          | Old API fields                                                                                 | Description                                                                                                                                                                                        |
|---------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| "country": {"code":integer, "id":integer, "name":string, "region":{"id":integer, "name":string}                                                         | "country": {"id": 0,"name": "string" }                                                         | In the old API, only the id and name of country was returned, whereas in the new API, the country code, name, id and nested region is also returned.                                               |
| "end_date": "2016-01-01"                                                                                                                                | "date": "2021-02-15"                                                                           | THe old API only returned the date, the new API returns the start and end date.                                                                                                                    |
| "latitude": "0","longitude": "0"                                                                                                                       | "global_coordinates": {"latitude": 0,"longitude": 0}                                           | The old API returned the latitude and longitude in the global coordinates field, but the new API return the latitude and longitude without it.                                                     |
| "perpetrators": \[{"country": null,"name": string}]                                                                                                     | "perpetrator": {"id": 0,"name": "string","country": {"id": 0,"name": "string"}}                | The old API returned perpetrators id name and nested country. The new one returns only the name and country.                                                                                       |
| "source_urls": \["string"]                                                                                                                              | "source_urls": "string"                                                                        | The old API returned source urls in the form of a string, while the new one returns it in the form of a list of strings.                                                                           |
| "start_date": "2016-01-01"                                                                                                                              | "date": "2021-02-15"                                                                           | THe old API only returned the date, the new API returns the start and end date.                                                                                                                    |
| "summary": string                                                                                                                                       | "event_summary": "string"                                                                      | The old API returned the summary using "event_summary" field. The new one use "summary" field.                                                                                                     |
| "targets": \[string]                                                                                                                                    | "target_types": \[{"id": 0,"name": "string" }]                                                 | The old API returns "target_types" field as a list of objects with id and name. The new just returns "targets" as a list of strings.                                                                                    |
| "victims": { "by_nationality": \[{ "deaths": 0,"hostages": 0,"injured": 1,"nationality": string}],"totals": {"deaths": 0,"hostages": 0,"injured": 0}}   | "victim_nationalities": \[{"nationality": "string","deaths": 0,"injured": 0,"hostages": 0 }]   | The old API returned "victim_nationalities" with nationality, deaths, injured and hostages. The new one returns "victims" with nationality, deaths, injured, hostages and totals of these all too. |
| "weapons": \[string]                                                                                                                                    | "weapon_types": \[{"id": 0,"name": "string"}]                                                  | The old API returns the "weapon_types" with id and string. The new API returns "weapons" in the form of a list of strings.                                                                         |
 
 #### Newly added fields
| Field name        | Type   | Description                                                                                       |
|-------------------|--------|---------------------------------------------------------------------------------------------------|
| area              | string | Brief address/location of the area were incident occurred. e.g "Stade de France, St Denis, Paris" |
| end_time          | string | The time at which the incident started. e.g '2153hrs'                                             |
| end_time_lookup   | string | e.g 'noon', 'morning' etc                                                                         |
| start_time        | string | The time at which the incident ended. e.g '2120hrs'                                               |
| start_time_lookup | string | e.g 'evening', 'morning' etc                                                                      |
___
## ~~Info for a specific incident~~
Not present in new API
___

## Plots
Fetch all plots between specified dates. Filter result using country_id or country_name. Paginate through results using _page_ and _limit_ parameters.

##### Old vs New API curl command.

| Old                                                                                                             | New                                                               |
|-----------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------|
| `curl -X GET https://client-apis.dragonflyintelligence.com/core/v1/plots -H 'Accept: application/json' -H 'API-KEY: API_KEY'` | `GET https://client-apis.dragonflyintelligence.com/core/v1/plots?from_date=1-1-2020&to_date=1-1-2000` |

#### Query Parameters
| Parameter     | In    | Description                         | Type           | Required |
|---------------|-------|-------------------------------------|----------------|----------|
| page          | query | Page number of results              | integer        | False    |
| limit         | query | Number of results per page          | integer        | False    |
| from_date     | query | Results newer than given date       | string         | True     |
| to_date       | query | Results older than given date       | string         | True     |
| sort          | query | Single field sort by.               | string         | False    |
| order         | query | Ordering to use for the sort field  | string         | False    |
| country_ids   | query | Country ids to filter results by    | Array[integer] | False    |
| country_names | query | Country names to filter results by. | Array[string]  | False    |

#### Example
Query all plots between 2000 and 2020.

`GET https://client-apis.dragonflyintelligence.com/core/v1/plots?from_date=1-1-2020&to_date=1-1-2000`

##### Example result
 This is a plot returned by the API.
 
 ```json
{
   "data":[
      {
         "area":"Orleans, Loiret, Centre-Val de Loire",
         "country":{
            "code":"FRA",
            "id":271,
            "name":"France",
            "region":{
               "id":264,
               "name":"Europe"
            }
         },
         "delivery_method":"Unknown",
         "event_type":"Other/Unclear",
         "id":1061,
         "latitude":"47.902906",
         "longitude":"1.902985",
         "perpetrators":[
            {
               "country":{
                  "code":"IRQ",
                  "id":362,
                  "name":"Iraq",
                  "region":{
                     "id":534,
                     "name":"Middle East"
                  }
               },
               "name":"Islamic State (IS)"
            }
         ],
         "planning_stage":"Unknown",
         "reported_at":"2015-12-16",
         "source_urls":[
            "http://s1.lemde.fr/assets-redaction/pdf/radiographie_du_terrorisme_made_in_France.pdf   https://www.larep.fr/orleans/loisirs/faits-divers/2015/12/24/attentat-dejoue-a-orleans-les-deux-hommes-voulaient-tuer-des-forces-de-l-ordre-a-leur-travail_11719060.htm"
         ],
         "specific_target":null,
         "summary":"Police arrested a Togolese national and a Moroccan national for planning attacks against the police and the military, in Orleans, Loiret department, Centre-Val de Loire region, on 16 December. Police did not reveal how advanced the plot was. Local media outlets reported that the individuals had pledged allegiance to Islamic State (IS).",
         "targets":[
            "Military",
            "Police and prisons"
         ],
         "weapons":[
            "Other/Unclear"
         ]
      }
   ]
}
```

#### CSV Format
 The new API can return incidents in CSV format also. Following are the headers of the comma separated values.

- id
- summary
- specific_target
- source_names
- source_urls
- reported_at
- country
- country_code
- region
- latitude
- longitude
- area
- targets
- weapons
- perpetrators
- event_type
- planning_stage
- delivery_method

#### Example
A plot returned in CSV format.

```json
{
  "text/csv": "id,summary,specific_target,source_names,source_urls,reported_at,country,country_code,region,latitude,longitude,area,targets,weapons,perpetrators,event_type,planning_stage,delivery_method,2,Police arrested a Togolese national and a Moroccan national for planning attacks against the police and the military,null,Journal du Cameroun; TeboPost Media,https://www.journalducameroun.com/; https://www.journalducameroun.com/2,2015-09-22,Iraq,IRQ,Middle East,47.902906,1.902985,Orleans;Loiret;Centre-Val de Loire,Military,Other/Unclear,Islamic State (IS),Armed attack,Initial,Person borne"
}
```

#### Comparision of different fields
 This is a comparision of those old fields and new ones which are different from each other.
 
| New API fields                                                                                                                               | Old API fields                                                                            | Description |
|----------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|-------------|
| "country":{"code":string, "id":16, "name":string, "region":{"id":12, "name":string}},                                                        | "geographical_region": {"id": 0,"name": "string"},"country": {"id": 0, "name": "string"}, | The old API returns "geographical_region" field which contains nested region and nested country. THe new one returns "country" field which contains code, id, name and nested region.           |
| "delivery_method":string,                                                                                                                    | "delivery_method": {"id": 0,"name": "string"}                                             | The old API returns "delivery_method" field as an object which contains its id and name. The new one returns delivery_method as a string.            |
| "event_type":string                                                                                                                          | "event_type": {"id": 0,"name": "string"},                                                 | The old API returns "event_type" field as an object which contains its id and name. The new one returns event_type as a string.            |
| "latitude":"0.0", "longitude":"0.0"                                                                                                          | "global_coordinates": {"latitude": 0,"longitude": 0}                                      | The old API returns "global_coordinates" field as an object containing the latitude and longitude, but the new API return the latitude and longitude without it.            |
| "perpetrators":\[{"country":{"code":string, "id":13, "name":string, "region":{"id":9, "name":string}},"name":string},]                       | "perpetrator": {"id": 0, "name": "string","country": {"id": 0, "name": "string"}},        | The old API returns "perpetrator" field as an object which contains id, name and nested country. THe new one returns perpetrators name, nested country and nested region.         |
| "planning_stage":string                                                                                                                      | "planning_stage": {"id": 0, "name": "string"},                                            | The old API returns an object containing id and name. The new one returns a string.            |
| "reported_at":"2019-07-30"                                                                                                                   | "date_reported": "2021-02-15"                                                             | The old API returns "date_reported" field. The new one returns "reported_at" field.            |
| "source_names":\["string"]                                                                                                                   | "source_names": "string"                                                                  | The old API returns a string. The new one returns a list of strings.            |
| "source_urls":\["string"]                                                                                                                    | "source_urls": "string"                                                                   | The old API returns a string. The new one returns a list of strings.            |
| "targets":\["string"]                                                                                                                        | "target_types": \[{"id": 0, "name": "string"}]                                            | The old API returns "target_types" field as a list of objects with id and name. The new just returns "targets" field as a list of strings.            |
| "weapons":\["string"]                                                                                                                        | "weapon_types": [{"id": 0, "name": "string"}                                              | The old API returns "weapon_types" field as a list of objects with id and name. The new just returns "weapons" field as a list of strings.            |

#### Newly added fields
| Field name        | Type   | Description                                                                                       |
|-------------------|--------|---------------------------------------------------------------------------------------------------|
| area              | string | Brief address/location of the area were plots occurred. e.g "Islamabad, Pakistan"                 |

## ~~Info for a specific plot~~
Not present in new API
___
**The rest of the endpoints are removed from new API.**
___

