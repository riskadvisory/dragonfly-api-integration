setup_for_tests() {
  local env="$1"
  local cert="$2"

  echo "$env" | base64 -d > .env
  echo "$cert" | base64 -d > certs/private.key

  pip install -r requirements.txt
  pip install -r tests/req_test.txt
}

run_tests() {
  pytest -n auto
}
