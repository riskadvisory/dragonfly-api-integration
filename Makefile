run:
	. cicd.sh
	setup_for_tests $ENV $CERT
	run_tests

test_local:
	. ./tests/variables.sh
	export ENV=$ENV_LOCAL
	export CERT=$CERT_LOCAL
	make run