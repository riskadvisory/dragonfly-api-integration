import json

from get_countries_geojson_5 import get_countries_geojson
from get_countries_json_7 import get_countries_json


class TestGetCountries:
    def test_get_json(self):
        file_path = get_countries_json()
        with open(file_path, 'r') as countries:
            resp = json.load(countries)
            assert len(resp['data']) >= 211

    def test_get_geojson(self):
        file_path = get_countries_geojson()
        with open(file_path, 'r') as countries:
            resp = json.load(countries)
            assert len(resp['features']) >= 211
            assert resp['type'] == 'FeatureCollection'
