from datetime import date
import json
from get_intelligence_report_json_11 import get_intelligence_reports_json


class TestGetIntelligenceReportsJson:

    def test_get_json(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 1
        file_path = get_intelligence_reports_json(from_date, to_date, page)
        with open(file_path, 'r') as intelligence_reports_file:
            resp = json.load(intelligence_reports_file)
        expected_metadata = {
            'count': resp['metadata']['count'],
            'limit': 10,
            'next': (
                f'/v1/intelligence_reports?page=2&from_date={from_date}&to_date={to_date}'
                f'&sort=reported_at&order=desc&limit=10'
            ),
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 10
        assert resp['metadata']['count'] >= 2457

    def test_get_pagination_next_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 1
        file_path = get_intelligence_reports_json(from_date, to_date, page)
        with open(file_path, 'r') as intelligence_reports_file:
            resp = json.load(intelligence_reports_file)
        next = f'/v1/intelligence_reports?page=2&from_date={from_date}&to_date={to_date}' \
               f'&sort=reported_at&order=desc&limit=10'
        assert resp['metadata']['next'] == next
        assert len(resp['data']) == 10

    def test_get_pagination_wrong_next_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 1
        file_path = get_intelligence_reports_json(from_date, to_date, page)
        with open(file_path, 'r')as intelligence_reports_file:
            resp = json.load(intelligence_reports_file)
        next = f'/v1/intelligence_reports?page=1&from_date=2022-10-01&to_date={to_date}' \
               f'&sort=reported_at&order=desc&limit=10'
        assert resp['metadata']['next'] != next
        assert len(resp['data']) == 10

    def test_get_pagination_previous_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 2
        file_path = get_intelligence_reports_json(from_date, to_date, page)
        with open(file_path, 'r')as intelligence_reports_file:
            resp = json.load(intelligence_reports_file)
        previous = f'/v1/intelligence_reports?page=1&from_date={from_date}&to_date={to_date}' \
                   f'&sort=reported_at&order=desc&limit=10'
        assert resp['metadata']['prev'] == previous
        assert len(resp['data']) == 10

    def test_get_pagination_wrong_previous_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 2
        file_path = get_intelligence_reports_json(from_date, to_date, page)
        with open(file_path, 'r') as intelligence_reports_file:
            resp = json.load(intelligence_reports_file)
        previous = f'/v1/intelligence_reports?page=2&from_date={from_date}&to_date={to_date}' \
                   f'&sort=reported_at&order=desc&limit=20'
        assert resp['metadata']['prev'] != previous
        assert len(resp['data']) == 10

    def test_get_pagination_check_page_number_in_next_and_prev_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 3
        file_path = get_intelligence_reports_json(from_date, to_date, page)
        with open(file_path, 'r') as intelligence_reports_file:
            resp = json.load(intelligence_reports_file)

        next = (
            f'/v1/intelligence_reports?page={page + 1}&from_date={from_date}&'
            f'to_date={to_date}&sort=reported_at&order=desc&limit=10'
        )
        previous = (
            f'/v1/intelligence_reports?page={page - 1}&from_date={from_date}&'
            f'to_date={to_date}&sort=reported_at&order=desc&limit=10'
        )
        assert resp['metadata']['next'] == next
        assert resp['metadata']['prev'] == previous
        assert len(resp['data']) == 10
