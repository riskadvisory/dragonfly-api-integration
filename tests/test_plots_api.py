import csv
from datetime import date, timedelta
import json

from get_plots_csv_13 import get_plots_csv
from get_plots_json_10 import get_plots_json
from make_request import make_call_json
from settings import DRAGONFLY_HOST


class TestGetPlotsJson:

    def test_get_json(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 1
        file_path = get_plots_json(from_date, to_date, page)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)
        expected_metadata = {
            'count': resp['metadata']['count'],
            'limit': 10,
            'next': (
                f'/v1/plots?page=2&from_date={from_date}&to_date={to_date}&sort=reported_at&order=desc&limit=10'
            ),
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 10
        assert resp['metadata']['count'] >= 518

    def test_get_pagination_next_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 1
        file_path = get_plots_json(from_date, to_date, page)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)
        next = f'/v1/plots?page=2&from_date={from_date}&to_date={to_date}&sort=reported_at&order=desc&limit=10'
        assert resp['metadata']['next'] == next
        assert len(resp['data']) == 10

    def test_get_pagination_wrong_next_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 1
        file_path = get_plots_json(from_date, to_date, page)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)
        next = f'/v1/plots?page=1&from_date=2022-10-01&to_date={to_date}&sort=reported_at&order=desc&limit=10'
        assert resp['metadata']['next'] != next
        assert len(resp['data']) == 10

    def test_get_pagination_previous_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 2
        file_path = get_plots_json(from_date, to_date, page)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)
        previous = f'/v1/plots?page=1&from_date={from_date}&to_date={to_date}&sort=reported_at&order=desc&limit=10'
        assert resp['metadata']['prev'] == previous
        assert len(resp['data']) == 10

    def test_get_pagination_wrong_previous_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 2
        file_path = get_plots_json(from_date, to_date, page)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)
        previous = f'/v1/plots?page=2&from_date={from_date}&to_date={to_date}&sort=reported_at&order=desc&limit=20'
        assert resp['metadata']['prev'] != previous
        assert len(resp['data']) == 10

    def test_get_pagination_check_page_number_in_next_and_prev_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 3
        file_path = get_plots_json(from_date, to_date, page)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)

        next = (
            f'/v1/plots?page={page + 1}&from_date={from_date}&'
            f'to_date={to_date}&sort=reported_at&order=desc&limit=10'
        )
        previous = (
            f'/v1/plots?page={page - 1}&from_date={from_date}&'
            f'to_date={to_date}&sort=reported_at&order=desc&limit=10'
        )
        assert resp['metadata']['next'] == next
        assert resp['metadata']['prev'] == previous
        assert len(resp['data']) == 10

    def test_get_filter_updated_from_date(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/plots'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'reported_at',
            'order': 'desc',
            'updated_from_date': '2010-01-01'
        }

        file_path = make_call_json(endpoint_name="plots", params=params, url=url)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)

        expected_metadata = {
            'count': resp['metadata']['count'],
            'limit': 10,
            'next': (
                f'/v1/plots?page=2&from_date={from_date}&to_date={to_date}&sort=reported_at&order=desc&'
                f'updated_from_date=2010-01-01&limit=10'
            ),
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 10
        assert resp['metadata']['count'] >= 523

    def test_get_filter_updated_from_date_when_future_date(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/plots'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'reported_at',
            'order': 'desc',
            'updated_from_date': f'{date.today() + timedelta(days=1):%Y-%m-%d}'
        }

        file_path = make_call_json(endpoint_name="plots", params=params, url=url)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0

    def test_get_filter_updated_to_date(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/plots'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'reported_at',
            'order': 'desc',
            'updated_to_date': to_date
        }

        file_path = make_call_json(endpoint_name="plots", params=params, url=url)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)

        expected_metadata = {
            'count': resp['metadata']['count'],
            'limit': 10,
            'next': (
                f'/v1/plots?page=2&from_date={from_date}&to_date={to_date}&sort=reported_at&order=desc&'
                f'updated_to_date={to_date}&limit=10'
            ),
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 10
        assert resp['metadata']['count'] >= 1600

    def test_get_filter_updated_to_date_when_too_old_date(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/plots'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'reported_at',
            'order': 'desc',
            'updated_to_date': '1998-02-20'
        }

        file_path = make_call_json(endpoint_name="plots", params=params, url=url)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0

    def test_get_filter_by_country_ids_when_non_existing_ids(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/plots'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'reported_at',
            'order': 'desc',
            'country_ids': [000]
        }

        file_path = make_call_json(endpoint_name="plots", params=params, url=url)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0

    def test_get_filter_by_country_names_when_non_existing_names(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/plots'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'reported_at',
            'order': 'desc',
            'country_names': ['abc', 'xyz']
        }

        file_path = make_call_json(endpoint_name="plots", params=params, url=url)
        with open(file_path, 'r') as plots_file:
            resp = json.load(plots_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0


class TestGetPlotsCsv:

    def test_get_csv(self):
        from_date = '2022-03-01'
        to_date = '2022-04-01'

        summary = (
            'Security forces thwarted a plot to attack civilians during Ramadan in unconfirmed locations of '
            'Salah Al-Din, Anbar and Nineveh governorate on 30 March. Security forces said the attack would '
            'take place during Ramadan (2 April - 2 May) but did not specify the type of weapons used. '
            'They also said that Islamic State (IS) was responsible for the plot.'
        )

        file_path = get_plots_csv(from_date, to_date)
        field_names = [
            'id', 'summary', 'specific_target', 'source_names', 'source_urls',
            'reported_at', 'country', 'country_code', 'region', 'latitude', 'longitude', 'area',
            'targets', 'weapons', 'perpetrators', 'event_type', 'planning_stage', 'delivery_method', 'updated_at',
            'created_at', 'tnt_equivalent_in_kg', 'connected_attacks',
            'target_nationalities', 'foreign_links', 'author', 'deleted_at', 'attributions', 'ambiguous'
        ]
        row_data = {
            'id': '66911',
            'summary': (
                'Security forces thwarted a plot to attack civilians during Ramadan in unconfirmed locations of '
                'Salah Al-Din, Anbar and Nineveh governorate on 30 March. Security forces said the attack would'
                ' take place during Ramadan (2 April - 2 May) but did not specify the type of weapons used. '
                'They also said that Islamic State (IS) was responsible for the plot.'
            ),
            'specific_target': 'Civilians during Ramadan', 'source_names': 'Shafaq; Al Araby',
            'source_urls': (
                'https://shafaq.com/ar/%D8%A3%D9%85%D9%80%D9%86/%D8%A7%D9%84%D8%B9%D9%85%D9%84%D9%8A%D8%A7%D8%AA'
                '-%D8%A7%D9%84%D9%85%D8%B4%D8%AA%D8%B1%D9%83%D8%A9-%D9%84%D8%B4%D9%81%D9%82-%D9%86%D9%8A%D9%88%D8'
                '%B2-%D8%AD%D8%A8%D8%A7%D8%B7-%D9%85%D8%AE%D8%B7%D8%B7-%D8%A7%D8%B1%D9%87%D8%A7%D8%A8%D9%8A-'
                '%D9%84%D8%A7%D8%B3%D8%AA%D9%87%D8%AF%D8%A7%D9%81-%D8%A7%D9%84%D9%85%D8%AF%D9%86%D9%8A%D9%8A'
                '%D9%86-%D9%81%D9%8A-%D8%B4%D9%87%D8%B1-%D8%B1%D9%85%D8%B6%D8%A7%D9%86;'
                ' https://www.alaraby.co.uk/politics/%D8%A7%D9%84%D9%82%D9%88%D8%A7%D8%AA'
                '-%D8%A7%D9%84%D8%B9%D8%B1%D8%A7%D9%82%D9%8A%D8%A9-%D8%AA%D8%AD%D8%A8%D8%B7-%D9%85'
                '%D8%AE%D8%B7%D8%B7-%22%D8%BA%D8%B2%D9%88%D8%A9-%D8%B1%D9%85%D8%B6%D8%A7%D9%86%22-%D9%84%D9%80%22%D8'
                '%AF%D8%A7%D8%B9%D8%B4%22'
            ),
            'reported_at': '2022-03-30', 'country': 'Iraq', 'country_code': 'IRQ', 'region': 'Middle East',
            'latitude': '34.399423', 'longitude': '43.471584', 'area': 'Salah Al-Din',
            'targets': 'Civilians/General Public', 'weapons': 'Undetermined',
            'perpetrators': 'Iraq:Islamic State (IS):Global Islamist', 'event_type': 'Undetermined',
            'planning_stage': 'Advanced', 'delivery_method': 'Undetermined',
            'updated_at': '2022-05-10 11:01:19+0000', 'created_at': '2022-04-01 11:01:08+0000',
            'tnt_equivalent_in_kg': '0.0', 'connected_attacks':'',
            'target_nationalities': 'Iraq:None:0', 'foreign_links': 'False', 'author': 'Flavien Baumgartner',
            'deleted_at': '', 'attributions': (
                'perpetrator_name: Islamic State (IS), '
                'perpetrator_type: Global Islamist,'
                'is_suspected: False,'
                'is_officially_attributed: True'
            )
        }
        plots = csv.DictReader(open(file_path))
        assert plots.fieldnames == field_names
        row = list(filter(lambda row: row['summary'] == summary, plots))[0]
        assert row['summary'] == row_data['summary']
        assert row['specific_target'] == row_data['specific_target']
        assert row['source_names'] == row_data['source_names']
        assert row['source_urls'] == row_data['source_urls']
        assert row['reported_at'] == row_data['reported_at']
        assert row['country'] == row_data['country']
        assert row['country_code'] == row_data['country_code']
        assert row['region'] == row_data['region']
        assert row['event_type'] == row_data['event_type']
        assert row['latitude'] == row_data['latitude']
        assert row['longitude'] == row_data['longitude']
        assert row['area'] == row_data['area']
        assert row['planning_stage'] == row_data['planning_stage']
        assert row['targets'] == row_data['targets']
        assert row['weapons'] == row_data['weapons']
        assert row['perpetrators'] == row_data['perpetrators']
        assert row['delivery_method'] == row_data['delivery_method']
        assert row['tnt_equivalent_in_kg'] == row_data['tnt_equivalent_in_kg']
        assert row['connected_attacks'] == row_data['connected_attacks']
        assert row['target_nationalities'] == row_data['target_nationalities']
        assert row['foreign_links'] == row_data['foreign_links']
        assert row['author'] == row_data['author']
        assert row['deleted_at'] == row_data['deleted_at']
        assert row['attributions'] == row_data['attributions']
