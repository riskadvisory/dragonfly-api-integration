import json

from get_regions_6 import get_regions_geojson


class TestGetRegionsJson:
    def test_get_regions_json(self):
        file_path = get_regions_geojson()
        with open(file_path, 'r') as regions:
            resp = json.load(regions)
            assert len(resp['data']) == 12
