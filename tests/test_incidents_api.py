import csv
from datetime import date, timedelta
import json

from get_incidents_csv_3 import get_incidents_csv
from get_incidents_json_2 import get_incidents_json
from make_request import make_call_json
from settings import DRAGONFLY_HOST


class TestGetIncidentsJson:

    def test_get_json(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 1
        file_path = get_incidents_json(from_date, to_date, page)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)
        expected_metadata = {
            'count': resp['metadata']['count'],
            'limit': 10,
            'next': (
                f'/v1/incidents?page=2&from_date={from_date}&to_date={to_date}&sort=start_date&order=desc&limit=10'
            ),
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 10
        assert resp['metadata']['count'] >= 21843

    def test_get_pagination_next_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 1
        file_path = get_incidents_json(from_date, to_date, page)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)
        next = f'/v1/incidents?page=2&from_date={from_date}&to_date={to_date}&sort=start_date&order=desc&limit=10'
        assert resp['metadata']['next'] == next
        assert len(resp['data']) == 10

    def test_get_pagination_wrong_next_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 1
        file_path = get_incidents_json(from_date, to_date, page)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)
        next = f'/v1/incidents?page=1&from_date=2022-10-01&to_date={to_date}&sort=start_date&order=desc&limit=10'
        assert resp['metadata']['next'] != next
        assert len(resp['data']) == 10

    def test_get_pagination_previous_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 2
        file_path = get_incidents_json(from_date, to_date, page)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)
        previous = f'/v1/incidents?page=1&from_date={from_date}&to_date={to_date}&sort=start_date&order=desc&limit=10'
        assert resp['metadata']['prev'] == previous
        assert len(resp['data']) == 10

    def test_get_pagination_wrong_previous_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 2
        file_path = get_incidents_json(from_date, to_date, page)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)
        previous = f'/v1/incidents?page=2&from_date={from_date}&to_date={to_date}&sort=start_date&order=desc&limit=20'
        assert resp['metadata']['prev'] != previous
        assert len(resp['data']) == 10

    def test_get_pagination_check_page_number_in_next_and_prev_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 3
        file_path = get_incidents_json(from_date, to_date, page)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)

        next = (
            f'/v1/incidents?page={page + 1}&from_date={from_date}&'
            f'to_date={to_date}&sort=start_date&order=desc&limit=10'
        )
        previous = (
            f'/v1/incidents?page={page - 1}&from_date={from_date}&'
            f'to_date={to_date}&sort=start_date&order=desc&limit=10'
        )
        assert resp['metadata']['next'] == next
        assert resp['metadata']['prev'] == previous
        assert len(resp['data']) == 10

    def test_get_filter_updated_from_date(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/incidents'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'start_date',
            'order': 'desc',
            'updated_from_date': '2010-01-01'
        }

        file_path = make_call_json(endpoint_name="incidents", params=params, url=url)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)

        expected_metadata = {
            'count': resp['metadata']['count'],
            'limit': 10,
            'next': (
                f'/v1/incidents?page=2&from_date={from_date}&to_date={to_date}&sort=start_date&order=desc&'
                f'updated_from_date=2010-01-01&limit=10'
            ),
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 10
        assert resp['metadata']['count'] >= 21843

    def test_get_filter_updated_from_date_when_future_date(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/incidents'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'start_date',
            'order': 'desc',
            'updated_from_date': f'{date.today() + timedelta(days=1):%Y-%m-%d}'
        }

        file_path = make_call_json(endpoint_name="incidents", params=params, url=url)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0

    def test_get_filter_updated_to_date(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/incidents'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'start_date',
            'order': 'desc',
            'updated_to_date': to_date
        }

        file_path = make_call_json(endpoint_name="incidents", params=params, url=url)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)

        expected_metadata = {
            'count': resp['metadata']['count'],
            'limit': 10,
            'next': (
                f'/v1/incidents?page=2&from_date={from_date}&to_date={to_date}&sort=start_date&order=desc&'
                f'updated_to_date={to_date}&limit=10'
            ),
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 10
        assert resp['metadata']['count'] >= 63000

    def test_get_filter_updated_to_date_when_too_old_date(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/incidents'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'start_date',
            'order': 'desc',
            'updated_to_date': '1998-02-20'
        }

        file_path = make_call_json(endpoint_name="incidents", params=params, url=url)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0

    def test_get_filter_by_country_ids_when_non_existing_ids(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/incidents'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'start_date',
            'order': 'desc',
            'country_ids': [000]
        }

        file_path = make_call_json(endpoint_name="incidents", params=params, url=url)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0

    def test_get_filter_by_country_names(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/incidents'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'start_date',
            'order': 'desc',
            'country_names': ['Zimbabwe', 'Yemen', 'Venezuela']
        }

        file_path = make_call_json(endpoint_name="incidents", params=params, url=url)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)

        expected_metadata = {
            'count': resp['metadata']['count'],
            'limit': 10,
            'next': (
                f'/v1/incidents?page=2&from_date={from_date}&to_date={to_date}&sort=start_date&order=desc&'
                f'country_names=Zimbabwe%2CYemen%2CVenezuela&limit=10'
            ),
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 10
        assert resp['metadata']['count'] >= 1139

    def test_get_filter_by_country_names_when_non_existing_names(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'

        url = f'{DRAGONFLY_HOST}/core/v1/incidents'
        params = {
            'from_date': from_date,
            'to_date': to_date,
            'page': 1,
            'limit': 10,
            'sort': 'start_date',
            'order': 'desc',
            'country_names': ['abc', 'xyz']
        }

        file_path = make_call_json(endpoint_name="incidents", params=params, url=url)
        with open(file_path, 'r') as incidents_file:
            resp = json.load(incidents_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0


class TestGetIncidentsCsv:

    def test_get_csv(self):
        from_date = '2022-03-01'
        to_date = '2022-04-01'
        summary = (
            'Gunmen attacked a military camp near Huddur, Huddur district, Bakool region, South West region on '
            'the morning of 1 March. There were no details on casualties. AT least three assailants were '
            'killed. Al-Shabaab claimed responsibility for the attack.'
        )
        file_path = get_incidents_csv(from_date, to_date)
        field_names = [
            'id', 'specific_target', 'suicide', 'terrorists_killed', 'summary',
            'source_urls', 'source_names', 'start_date', 'end_date',
            'start_time', 'start_time_lookup', 'end_time', 'end_time_lookup', 'event_type', 'latitude', 'longitude',
            'accuracy', 'area', 'country_code', 'country', 'region', 'targets', 'weapons',
            'total_deaths', 'total_hostages', 'total_injured',
            'deaths', 'hostages', 'injured', 'perpetrators', 'tnt_equivalent_in_kg',
            'delivery_method', 'connected_attacks', 'author', 'updated_at', 'created_at', 'deleted_at',
            'attributions', 'ambiguous'
        ]
        row_data = {
            'id': '63443', 'specific_target': 'Military camp', 'suicide': 'False', 'terrorists_killed': '3',
            'summary': (
                'Gunmen attacked a military camp near Huddur, Huddur district, Bakool region, South West region on '
                'the morning of 1 March. There were no details on casualties. AT least three assailants were '
                'killed. Al-Shabaab claimed responsibility for the attack.'
            ),
            'source_urls': 'https://radiorisaala.com/251295-2/; https://twitter.com/HarunMaruf/status/1498651102090256390',
            'source_names': 'Radio Risaala; Harun Maruf', 'start_date': '2022-03-01', 'end_date': '2022-03-01',
            'start_time': 'Morning', 'start_time_lookup': 'morning', 'end_time': 'Morning',
            'end_time_lookup': 'morning', 'event_type': 'Armed attack', 'latitude': '4.1228',
            'longitude': '43.88881', 'accuracy': '100-200m', 'area': 'Huddur, Huddur, Bakool, South West',
            'country_code': 'SOM', 'country': 'Somalia', 'region': 'East and Southern Africa', 'targets': 'Military',
            'weapons': 'Firearms', 'total_deaths': '0', 'total_hostages': '0', 'total_injured': '0',
            'deaths': 'Somalia: 0', 'hostages': 'Somalia: 0', 'injured': 'Somalia: 0',
            'perpetrators': 'Somalia:Al-Shabaab:Global Islamist', 'tnt_equivalent_in_kg': '0.0',
            'delivery_method': 'Undetermined', 'connected_attacks': '', 'author': 'Flavien Baumgartner', 'updated_at': '2022-05-04 06:39:55+0000',
            'created_at': '2022-03-01 15:00:22+0000', 'deleted_at': '',
            'attributions': (
                'perpetrator_name: Al-Shabaab, perpetrator_type: Global Islamist,is_claimed: True,'
                ' is_suspected: False,is_officially_attributed: False'
            )
        }
        incidents = csv.DictReader(open(file_path))
        assert incidents.fieldnames == field_names
        row = list(filter(lambda row: row['summary'] == summary, incidents))[0]
        assert row['specific_target'] == row_data['specific_target']
        assert row['suicide'] == row_data['suicide']
        assert row['terrorists_killed'] == row_data['terrorists_killed']
        assert row['source_urls'] == row_data['source_urls']
        assert row['summary'] == row_data['summary']
        assert row['source_urls'] == row_data['source_urls']
        assert row['source_names'] == row_data['source_names']
        assert row['start_date'] == row_data['start_date']
        assert row['end_date'] == row_data['end_date']
        assert row['start_time'] == row_data['start_time']
        assert row['start_time_lookup'] == row_data['start_time_lookup']
        assert row['end_time'] == row_data['end_time']
        assert row['end_time_lookup'] == row_data['end_time_lookup']
        assert row['event_type'] == row_data['event_type']
        assert row['latitude'] == row_data['latitude']
        assert row['longitude'] == row_data['longitude']
        assert row['accuracy'] == row_data['accuracy']
        assert row['area'] == row_data['area']
        assert row['country_code'] == row_data['country_code']
        assert row['country'] == row_data['country']
        assert row['region'] == row_data['region']
        assert row['weapons'] == row_data['weapons']
        assert row['total_deaths'] == row_data['total_deaths']
        assert row['total_hostages'] == row_data['total_hostages']
        assert row['total_injured'] == row_data['total_injured']
        assert row['deaths'] == row_data['deaths']
        assert row['hostages'] == row_data['hostages']
        assert row['injured'] == row_data['injured']
        assert row['perpetrators'] == row_data['perpetrators']
        assert row['tnt_equivalent_in_kg'] == row_data['tnt_equivalent_in_kg']
        assert row['delivery_method'] == row_data['delivery_method']
        assert row['connected_attacks'] == row_data['connected_attacks']
        assert row['author'] == row_data['author']
        assert row['deleted_at'] == row_data['deleted_at']
        assert row['attributions'] == row_data['attributions']
