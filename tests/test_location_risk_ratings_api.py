from datetime import date
import json
from get_location_risk_ratings_12 import get_location_risk_ratings_json
from make_request import make_call_json
from settings import DRAGONFLY_HOST


class TestGetLocationRiskRatingsJson:

    def test_get_json(self):
        page = 1
        file_path = get_location_risk_ratings_json(page)
        with open(file_path, 'r') as location_risk_ratings_file:
            resp = json.load(location_risk_ratings_file)
        expected_metadata = {
            'count': resp['metadata']['count'],
            'limit': 10,
            'next': (
                f'/v1/risk_ratings?page=2&limit=10'
            ),
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 10
        assert resp['metadata']['count'] >= 5541

    def test_get_pagination_next_url(self):
        page = 1
        file_path = get_location_risk_ratings_json(page)
        with open(file_path, 'r') as location_risk_ratings_file:
            resp = json.load(location_risk_ratings_file)
        next = f'/v1/risk_ratings?page=2&limit=10'
        assert resp['metadata']['next'] == next
        assert len(resp['data']) == 10

    def test_get_pagination_wrong_next_url(self):
        page = 1
        file_path = get_location_risk_ratings_json(page)
        with open(file_path, 'r') as location_risk_ratings_file:
            resp = json.load(location_risk_ratings_file)
        next = f'/v1/risk_ratings?page=1&limit=10'
        assert resp['metadata']['next'] != next
        assert len(resp['data']) == 10

    def test_get_pagination_previous_url(self):
        page = 2
        file_path = get_location_risk_ratings_json(page)
        with open(file_path, 'r') as location_risk_ratings_file:
            resp = json.load(location_risk_ratings_file)
        previous = f'/v1/risk_ratings?page=1&limit=10'
        assert resp['metadata']['prev'] == previous
        assert len(resp['data']) == 10

    def test_get_pagination_wrong_previous_url(self):
        from_date = '2002-02-20'
        to_date = f'{date.today():%Y-%m-%d}'
        page = 2
        file_path = get_location_risk_ratings_json(page)
        with open(file_path, 'r') as location_risk_ratings_file:
            resp = json.load(location_risk_ratings_file)
        previous = f'/v1/risk_ratings?page=2&limit=20'
        assert resp['metadata']['prev'] != previous
        assert len(resp['data']) == 10

    def test_get_pagination_check_page_number_in_next_and_prev_url(self):
        page = 3
        file_path = get_location_risk_ratings_json(page)
        with open(file_path, 'r') as location_risk_ratings_file:
            resp = json.load(location_risk_ratings_file)

        next = f'/v1/risk_ratings?page={page + 1}&limit=10'
        previous = f'/v1/risk_ratings?page={page - 1}&limit=10'
        assert resp['metadata']['next'] == next
        assert resp['metadata']['prev'] == previous
        assert len(resp['data']) == 10

    def test_get_filter_by_location_ids_when_non_existing_ids(self):
        url = f'{DRAGONFLY_HOST}/core/v1/risk_ratings'
        params = {
            'page': 1,
            'limit': 10,
            'location_ids': [000]
        }
        file_path = make_call_json(endpoint_name="risk_ratings", params=params, url=url)
        with open(file_path, 'r') as location_risk_ratings_file:
            resp = json.load(location_risk_ratings_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0

    def test_get_filter_by_country_names_when_non_existing_names(self):
        url = f'{DRAGONFLY_HOST}/core/v1/risk_ratings'
        params = {
            'page': 1,
            'limit': 10,
            'location_names': ['abc', 'xyz']
        }

        file_path = make_call_json(endpoint_name="risk_ratings", params=params, url=url)
        with open(file_path, 'r') as location_risk_ratings_file:
            resp = json.load(location_risk_ratings_file)

        expected_metadata = {
            'count': 0,
            'limit': 10,
            'next': None,
            'page': 1,
            'prev': None
        }
        assert resp['metadata'] == expected_metadata
        assert len(resp['data']) == 0
