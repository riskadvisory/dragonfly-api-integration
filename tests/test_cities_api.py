import json

from get_cities_9_geojson import get_cities_geojson


class TestGetCitiesGeoJson:
    def test_get_cities_geojson(self):
        file_path = get_cities_geojson()
        with open(file_path, 'r') as cities:
            resp = json.load(cities)
            assert len(resp['features']) >= 710
            assert resp['type'] == 'FeatureCollection'
