# Transitional guide of new API's  (SIAS) 

## Authentication

#### Dragonfly apis uses JWT based authentication.

Go through `README.md` for generating JWT token.

Pass JWT token in authorization header in your request to the endpoint.

## Location API's

You can access data related to cities, countries and regions.

#### Cities
List all the cities. 

##### Old vs New API curl command.

Their was no endpoint for cities in the old API. Following is the curl command for the new one.

e.g ` GET https://client-apis.dragonflyintelligence.com/core/v1/cities`

#### Example
This is a sample result of cities endpoint.
```json
{
   "data":[
      {
         "country":{
            "code":null,
            "id":821,
            "name":"Switzerland",
            "region":{
               "id":263,
               "name":"Europe"
            }
         },
         "id":926,
         "name":"Zurich"
      },
      {
         "country":{
            "code":null,
            "id":174,
            "name":"China",
            "region":{
               "id":43,
               "name":"Asia Pacific"
            }
         },
         "id":924,
         "name":"Zhengzhou"
      }
   ]
}
```

##### Comparision of New API fields with Old ones.
_~~The Old one did not have any Endpoint for cities.~~_

#### Countries
List all the countries.

##### Old vs New API curl command.
| Old                                                                                                                 | New                                  |
|---------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| `curl -X GET https://api.terrorismtracker.com/api/v1/countries -H 'Accept: application/json' -H 'API-KEY: API_KEY'` | `curl GET https://client-apis.dragonflyintelligence.com/core/v1/countries` |

##### Comparison between new and old fields
| New API field                                               | Old API field                                        | Description                                                                                |
|-------------------------------------------------------------|------------------------------------------------------|--------------------------------------------------------------------------------------------|
| `"region": {"id": 248, "name": "East &amp; Southern Africa"}` |  `"geographical_region": {"id": 0, "name": "string"}}` | The Old API returns "region" field as an object. The New API returns "geographical_region" |
|                                                             |                                                      |                                                                                            |
##### Newly added fields in New API

| Field | Description             |
|-------|-------------------------|
| code  | A code for the country. |

##### Example result
Two countries result returned by /countries endpoint.


```json
{
   "data":[
      {
         "code":null,
         "id":925,
         "name":"Zimbabwe",
         "region":{
            "id":248,
            "name":"East &amp; Southern Africa"
         }
      },
      {
         "code":null,
         "id":922,
         "name":"Zambia",
         "region":{
            "id":248,
            "name":"East &amp; Southern Africa"
         }
      }
   ]
}
```
#### Regions
List all the regions.

##### Old vs New API curl command.

| Old                                                                                                                            | New                                |
|--------------------------------------------------------------------------------------------------------------------------------|------------------------------------|
| `curl -X GET https://api.terrorismtracker.com/api/v1/geographical_regions -H 'Accept: application/json' -H 'API-KEY: API_KEY'` | `curl GET https://client-apis.dragonflyintelligence.com/core/v1/regions` |

##### Comparison between new and old fields

| New API field                                     | Old API field                                                           | Description                                                                                           |
|---------------------------------------------------|-------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| `{"id": 905, "name": "West &amp; Central Africa"}` | `{"id": 0, "name": "string", "countries": [{"id": 0, "name": "string"}]}` | THe Old API returns region's id, name and nested countries. The New API returns region's id and name. |

##### Example result
Two regions returned by /regions endpoint

```json
{
   "data":[
      {
         "id":905,
         "name":"West &amp; Central Africa"
      },
      {
         "id":931,
         "name":"Sub-Saharan Africa"
      }
   ]
}
```
---

## Intelligence Reports

Get SIAS intelligence reports by date.

##### Old vs New API curl command.
 
| Old                                                    | New                                                                                                |
|--------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| `GET /v1/intelligence-reports X-API-Key: YOUR_API_KEY` | ` GET https://client-apis.dragonflyintelligence.com/core/v1/intelligence_reports?page=1&limit=10&from_date=2-2-2014&to_date=2-2-2015` |


#### Query Parameters  

| Parameter | Type    | Required | Description                        |
|-----------|---------|----------|------------------------------------|
| page      | integer | False    | Page number of results             |
| limit     | integer | False    | Number of results per page         |
| from_date | string  | True     | Results newer than given date      |
| to_date   | string  | True     | Results older than given date      |
| sort      | string  | True     | Single field sort by               |
| order     | string  | False    | Ordering to use for the sort field |

#### Example
Fetching all SIAS intelligence reports **from 2014 to 2015**, **10** results at a time.
 
` GET https://client-apis.dragonflyintelligence.com/core/v1/intelligence_reports?page=1&limit=10&from_date=2-2-2014&to_date=2-2-2015`

##### Comparison between new and old fields

| Old API                       | New API                              | Description                                                                             |
|-------------------------------|--------------------------------------|-----------------------------------------------------------------------------------------|
| "date": "2019-07-10 14:00:30" | "reported_at": "2015-12-29 16:05:44" | The Old API returns "date" field while the new one returns date in "reported_at" field. |

##### Newly added fields in New API
These are the newly added fields in Dragonfly. 

| Field       | Description                                                                                                                                                                                                           |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| cities      | A list of cities linked with the intelligence report.                                                                                                                                                                 |
| countries   | A list of countries linked with the intelligence report. It contains the country code, id, name and nested region. e.g  `{"code": null, "id": 52, "name": "Australia", "region": {"id": 43, "name": "Asia Pacific"}}` |
| image       | The link of an image associated with the report. e.g "https://sias.riskadvisory.net/app/uploads/2013/06/Bangladesh-600x352-e1385478941839-528x310.jpg"                                                                |
| regions     | List of regions with their id and name e.g `{"id": 796, "name": "South Asia"}`                                                                                                                                        |
| report_type | Type of report e.g Priority                                                                                                                                                                                           |
| reported_at |                                                                                                                                                                                                                       |
| risks       | The type of risk e.g Civil Repression, Governance Risk etc                                                                                                                                                            |
| sectors     | The sector associates with the report. e.g Education, Diplomatic Interests etc                                                                                                                                        |

##### Example result
An Intelligence report returned by /intelligence_reports endpoint.

```json
{
   "cities":[
      
   ],
   "content":"<p>The US Embassy in Dhaka is ......</p>",
   "countries":[
      {
         "code":null,
         "id":52,
         "name":"Australia",
         "region":{
            "id":43,
            "name":"Asia Pacific"
         }
      },
      {
         "code":null,
         "id":70,
         "name":"Bangladesh",
         "region":{
            "id":796,
            "name":"South Asia"
         }
      }
   ],
   "id":6874,
   "image":"https://sias.riskadvisory.net/app/uploads/2013/06/Bangladesh-600x352-e1385478941839-528x310.jpg",
   "regions":[
      {
         "id":796,
         "name":"South Asia"
      }
   ],
   "report_type":"Priority",
   "reported_at":"2015-12-29 16:05:44",
   "risks":[
      "Civil Repression",
      "Governance Risk",
      "Hotels Threats",
      "Terrorism"
   ],
   "sectors":[
      "Diplomatic Interests &amp; Embassies",
      "Education",
      "Humanitarian/NGO",
      "Tourism &amp; Hotels"
   ],
   "title":"Bangladesh | US warns of possible terrorist attacks"
}
```
---

## ~~Taxonomy~~

Not present in new API

---

## Travel Risk Monitor

Get SIAS TRM security intelligence reports by [Location id](#location-apis) or [Location name](#location-apis). Use following endpopint

##### Old vs New API curl command.

| Old           | New                                                                                 |
|---------------|-------------------------------------------------------------------------------------|
| `GET /v1/trm` | `GET https://client-apis.dragonflyintelligence.com/core/v1/travel_risk_monitor_alerts?page=1&limit=10&location_ids=261` |

#### Query Parameters  
page	Page number of results
limit	Number of results per page
location_ids	Location ids to filter results by
location_names	Location names to filter results by

| Parameter | Type    | Required | Description                        |
|-----------|---------|----------|------------------------------------|
| page      | integer | False    | Page number of results             |
| limit     | integer | False    | Number of results per page         |
| location_ids| Array[integer] | False  | Location ids to filter results by     |
| location_names| Array[string] | False| Location names to filter results by   |

#### Example
- Fetching all travel risk monitor alerts from **Ethiopia** using its location id, **10** results at a time.

`GET https://client-apis.dragonflyintelligence.com/core/v1/travel_risk_monitor_alerts?page=1&limit=10&location_ids=261` 

- Fetching all travel risk monitor alerts from **Ethiopia** using its location name, **5** results at a time. 

`GET  https://client-apis.dragonflyintelligence.com/core/v1/travel_risk_monitor_alerts?page=1&limit=5&location_names=Ethiopia ` 

##### Comparison between new and old fields

The travel risk monitor alert has totally been revamped in the new API. All the fields are replaced with new ones which are mentioned in the next section. THe only thing common between the two are the "title" and "id" fields.

##### Newly added fields in New API

| Field       | Description                                                                                    |
|-------------|------------------------------------------------------------------------------------------------|
| alerts      | The type of alert e.g Covid-19, Public Health Alert etc                                        |
| cities      | The cities for which the alert is issued.                                                      |
| countries   | THe countries for which the alert is issued. It contains the code, is, name and nested region. |
| description | A brief description of the alert.                                                              |
| end_date    | The end date for the alert.                                                                    |
| nationwide  | A Boolean value indicating whether the alert is for the whole nation or not.                   |
| regions     | The regions for which the alert is issued.                                                     |
| start_date  | Start date of the alert.                                                                       |
| reported_at | The date and time at which the alert was reported.                                             |

##### Example result

A TRM alert returned by the /travel_risk_monitor_alerts endpoint.

```json
{
   "data":[
      {
         "alerts":[
            "Election",
            "Official Travel Advisory"
         ],
         "cities":[
            
         ],
         "countries":[
            {
               "code":null,
               "id":261,
               "name":"Ethiopia",
               "region":{
                  "id":248,
                  "name":"East &amp; Southern Africa"
               }
            }
         ],
         "description":"The French government has advised citizens against all travel to Ethiopia from 26 June until early July due to the potential for unrest around the presidential election scheduled for 21 June. It advised against travel outside the capital.",
         "end_date":"2021-06-17",
         "id":36484,
         "nationwide":true,
         "regions":[
            
         ],
         "reported_at":"2021-06-17 13:24:00",
         "start_date":"2021-06-17",
         "title":"Ethiopia: French and US governments advise of potential violence over election"
      }
   ]
}
```

#### CSV Format
 The new API can return TRM alerts in CSV format also. Following are the headers of the comma separated values.

- id
- title
- description
- nationwide
- start_date
- end_date
- reported_at
- alerts
- regions
- countries
- cities

#### Example
A Travel risk monitor alert returned in CSV format.

```json
{
  "text/csv": "id,title,description,nationwide,start_date,end_date,reported_at,alerts,regions,countries,cities,36491,Maldives: Curfew shortened in the Greater Male region,\"The authorities have, MaldivesSource: Health Protection Agency\",False,2021-06-17,2021-06-17,2021-06-17 13:06:17,Covid-19; Curfew/Movement Restrictions; Public Health Alert,,Maldives:None,Male 36527,Ukraine: Demonstration in Kyiv,Unions representing\",False,2021-06-17,2021-06-17,2021-06-15 10:20:40,Demonstration/Strike,,Ukraine:None,Kyiv\n"
}
```
---

## ~~Risk Types~~

Not present in new API

---

## Risk Ratings

Get SIAS risk_ratings by [Location id](#location-apis) or [Location name](#location-apis). Use following endpoint

##### Old vs New API curl command.

| Old                                          | New                                                                           |
|----------------------------------------------|-------------------------------------------------------------------------------|
| `GET /v1/risk-rating?location_labels=LABELS` | ` GET https://client-apis.dragonflyintelligence.com/core/v1/risk_ratings?page=1&limit=2&location_names=Pakistan`  |


#### Query Parameters  

| Parameter | Type    | Required | Description                        |
|-----------|---------|----------|------------------------------------|
| page      | integer | False    | Page number of results             |
| limit     | integer | False    | Number of results per page         |
| location_ids| Array[integer] | False  | Location ids to filter results by     |
| location_names| Array[string] | False| Location names to filter results by   |


#### Example
Fetch risk ratings of **Pakistan**. **2** at a time.

` GET https://client-apis.dragonflyintelligence.com/core/v1/risk_ratings?page=1&limit=2&location_names=Pakistan `

##### Comparison between new and old fields
Risk ratings also have been completely revamped. The id field remains the same in both. THe rest of them have changed as mentioned in the following section.

##### Newly added fields in New API

| Field              | Description                                                          |
|--------------------|----------------------------------------------------------------------|
| location           | The location of the risk rating. It contains its id, name and type.  |
| rating             | An integer which indicates the severity of the risk.                 |
| rating_description | A brief description of the risk rating.                              |
| rating_name        | A string indicating the probability/severity of risk rating. e.g low |
| rating_reported_at | The date and time at which the risk was reported.                    |
| risk_type          | The type of risk. e.g Terrorism Risk                                 |

##### Example result
A risk rating return by /risk_ratings endpoint.

```json
{
  "data": [
    {
      "id": 16963,
      "location": {
        "id": 864,
        "name": "Tunisia",
        "type": "country"
      },
      "rating": 3,
      "rating_description": null,
      "rating_name": null,
      "rating_reported_at": "2021-06-16 16:09:00",
      "risk_type": "Terrorism Risk"
    }
  ]
}
```
#### CSV Format
 The new API can return Risk ratings in CSV format also. Following are the headers of the comma separated values.

- id
- risk_type
- rating
- rating_name
- rating_description
- rating_reported_at
- location_id
- location_name
- location_type

#### Example
A Risk Rating returned in CSV format.

```json
{
  "text/csv": "id,risk_type,rating,rating_name,rating_description,rating_reported_at,location_id,location_name,location_type 1146,Terrorism Risk,3,Moderate,We assess that exposure to an attack/s is a reasonable possibility. High-impact attacks are unlikely.,2021-03-18 12:30:00,252,Egypt,country 1183,Interstate Conflict,4,High / Substantial,\"There is a credible threat of military attack by, or armed conflict with, another state in the near to medium term. The most probable form of conflict is limited overseas action as part of a coalition, or a brief or limited conflict in a contained area.\",2021-03-03 18:42:00,261,Ethiopia,country\n"
}
```
